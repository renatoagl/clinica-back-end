package com.rco.clinica.diary;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "agenda")
@Data
public class Diary implements Serializable {

	private static final long serialVersionUID = 983638239734342383L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "dia")
	private String day;

	@Column(name = "hora_inicio")
	private String startTime;

	@Column(name = "hora_fin")
	private String endTime;

	@Column(name = "cupos")
	private Integer quotas;

	@Column(name = "activo")
	private String active;

	@Column(name = "medico_id")
	private Long user;
}
