package com.rco.clinica.diary;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("diaries")
public class DiaryController {

	private DiaryRepository diaryRepository;

	@Autowired
	DiaryController(DiaryRepository diaryRepository) {
		this.diaryRepository = diaryRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Diary>> getDiaries() {
		return new ResponseEntity<Iterable<Diary>>(diaryRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Diary> getDiaryById(@PathVariable("id") long id) {
		Optional<Diary> diary = diaryRepository.findById(id);
		if (diary.isPresent()) {
			return new ResponseEntity<Diary>(diary.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Diary>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<Diary> createDiary(@RequestBody Diary diaryRequest) {
		Diary diary = new Diary();
		diary.setDay(diaryRequest.getDay());
		diary.setStartTime(diaryRequest.getStartTime());
		diary.setEndTime(diaryRequest.getEndTime());
		diary.setQuotas(diaryRequest.getQuotas());
		diary.setActive(diaryRequest.getActive());
		diary.setUser(diaryRequest.getUser());

		diaryRepository.save(diary);
		return new ResponseEntity<Diary>(diary, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Diary> updateDiary(@PathVariable("id") long id, @RequestBody Diary diaryRequest) {
		Optional<Diary> diary = diaryRepository.findById(id);

		if (diary.isPresent()) {
			Diary diaryEntity = diary.get();
			diaryEntity.setDay(diaryRequest.getDay());
			diaryEntity.setStartTime(diaryRequest.getStartTime());
			diaryEntity.setEndTime(diaryRequest.getEndTime());
			diaryEntity.setQuotas(diaryRequest.getQuotas());
			diaryEntity.setActive(diaryRequest.getActive());
			diaryEntity.setUser(diaryRequest.getUser());

			diaryRepository.save(diaryEntity);
			return new ResponseEntity<Diary>(diaryEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Diary>(HttpStatus.NOT_FOUND);
		}
	}

}
