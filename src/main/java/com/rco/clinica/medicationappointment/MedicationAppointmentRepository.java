package com.rco.clinica.medicationappointment;

import org.springframework.data.repository.CrudRepository;

public interface MedicationAppointmentRepository extends CrudRepository<MedicationAppointment, Long> {

}
