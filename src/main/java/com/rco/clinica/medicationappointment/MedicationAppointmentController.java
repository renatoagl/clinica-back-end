package com.rco.clinica.medicationappointment;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("medication-appointments")
public class MedicationAppointmentController {

	private MedicationAppointmentRepository medicationAppointmentRepository;

	@Autowired
	MedicationAppointmentController(MedicationAppointmentRepository medicationAppointmentRepository) {
		this.medicationAppointmentRepository = medicationAppointmentRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<MedicationAppointment>> getMedicationAppointments() {
		return new ResponseEntity<Iterable<MedicationAppointment>>(medicationAppointmentRepository.findAll(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<MedicationAppointment> getMedicationAppointmentById(@PathVariable("id") long id) {
		Optional<MedicationAppointment> medicationAppointment = medicationAppointmentRepository.findById(id);
		if (medicationAppointment.isPresent()) {
			return new ResponseEntity<MedicationAppointment>(medicationAppointment.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<MedicationAppointment>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<MedicationAppointment> createMedicationAppointment(
			@RequestBody MedicationAppointment medicationAppointmentRequest) {
		MedicationAppointment medicationAppointment = new MedicationAppointment();
		medicationAppointment.setAppointment(medicationAppointmentRequest.getAppointment());
		medicationAppointment.setMedicine(medicationAppointmentRequest.getMedicine());

		medicationAppointmentRepository.save(medicationAppointment);
		return new ResponseEntity<MedicationAppointment>(medicationAppointment, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<MedicationAppointment> updateMedicationAppointment(@PathVariable("id") long id,
			@RequestBody MedicationAppointment medicationAppointmentRequest) {
		Optional<MedicationAppointment> medicationAppointment = medicationAppointmentRepository.findById(id);

		if (medicationAppointment.isPresent()) {
			MedicationAppointment medicationAppointmentEntity = medicationAppointment.get();
			medicationAppointmentEntity.setAppointment(medicationAppointmentRequest.getAppointment());
			medicationAppointmentEntity.setMedicine(medicationAppointmentRequest.getMedicine());

			medicationAppointmentRepository.save(medicationAppointmentEntity);
			return new ResponseEntity<MedicationAppointment>(medicationAppointmentEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<MedicationAppointment>(HttpStatus.NOT_FOUND);
		}
	}

}
