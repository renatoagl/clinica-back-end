package com.rco.clinica.medicationappointment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "citamedicamento")
@Data
public class MedicationAppointment implements Serializable {

	private static final long serialVersionUID = -1383636210272389790L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "cita_id")
	private String appointment;

	@Column(name = "medicamento_id")
	private String medicine;
}
