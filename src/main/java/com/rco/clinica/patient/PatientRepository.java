package com.rco.clinica.patient;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PatientRepository extends CrudRepository<Patient, String> {

	@Query(value = "SELECT * FROM paciente AS p WHERE (:text IS NULL OR (p.dni LIKE CONCAT('%',:text,'%') OR p.nombre LIKE CONCAT('%',:text,'%')))", nativeQuery = true)
	List<Patient> getAllPatientsFiltered(@Param("text") String text);

}
