package com.rco.clinica.patient;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "paciente")
@Data
public class Patient implements Serializable {

	private static final long serialVersionUID = -4958670891521391477L;

	@Id
	@Column(name = "DNI")
	private String dni;

	@Column(name = "nombre")
	private String name;

	@Column(name = "num_expediente")
	private Integer fileNumber;

	@Column(name = "telefono")
	private String telephone;

	@Column(name = "sexo")
	private String sex;

	@Column(name = "municipio")
	private String municipality;

	@Column(name = "provincia")
	private String province;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_nacimiento")
	private Date birthDate;

}
