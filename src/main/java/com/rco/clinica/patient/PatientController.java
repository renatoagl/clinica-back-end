package com.rco.clinica.patient;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("patients")
public class PatientController {

	private PatientRepository patientRepository;

	@Autowired
	PatientController(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Patient>> getPatients() {
		return new ResponseEntity<Iterable<Patient>>(patientRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{dni}")
	public ResponseEntity<Patient> getPatientById(@PathVariable("dni") String dni) {
		Optional<Patient> patient = patientRepository.findById(dni);
		if (patient.isPresent()) {
			return new ResponseEntity<Patient>(patient.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("")
	public ResponseEntity<List<Patient>> getPatientsByNifOrName(@RequestParam(required = false) String text) {
		return new ResponseEntity<List<Patient>>(patientRepository.getAllPatientsFiltered("%" + text + "%"),
				HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<Patient> createPatient(@RequestBody Patient patientRequest) {
		Patient patient = new Patient();
		patient.setDni(patientRequest.getDni());
		patient.setName(patientRequest.getName());
		patient.setFileNumber(patientRequest.getFileNumber());
		patient.setTelephone(patientRequest.getTelephone());
		patient.setSex(patientRequest.getSex());
		patient.setMunicipality(patientRequest.getMunicipality());
		patient.setProvince(patientRequest.getProvince());
		patient.setBirthDate(patientRequest.getBirthDate());

		patientRepository.save(patient);
		return new ResponseEntity<Patient>(patient, HttpStatus.CREATED);
	}

	@PutMapping("/{dni}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Patient> updatePatient(@PathVariable("dni") String dni, @RequestBody Patient patientRequest) {
		Optional<Patient> patient = patientRepository.findById(dni);

		if (patient.isPresent()) {
			Patient patientEntity = patient.get();
			patientEntity.setName(patientRequest.getName());
			patientEntity.setFileNumber(patientRequest.getFileNumber());
			patientEntity.setTelephone(patientRequest.getTelephone());
			patientEntity.setSex(patientRequest.getSex());
			patientEntity.setMunicipality(patientRequest.getMunicipality());
			patientEntity.setProvince(patientRequest.getProvince());
			patientEntity.setBirthDate(patientRequest.getBirthDate());

			patientRepository.save(patientEntity);
			return new ResponseEntity<Patient>(patientEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
		}
	}

}
