package com.rco.clinica.appointment;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("appointments")
public class AppointmentController {

	private AppointmentRepository appointmentRepository;

	@Autowired
	AppointmentController(AppointmentRepository appointmentRepository) {
		this.appointmentRepository = appointmentRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Appointment>> getAppointments() {
		return new ResponseEntity<Iterable<Appointment>>(appointmentRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Appointment> getAppointmentById(@PathVariable("id") long id) {
		Optional<Appointment> appointment = appointmentRepository.findById(id);
		if (appointment.isPresent()) {
			return new ResponseEntity<Appointment>(appointment.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Appointment>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<Appointment> createAppointment(@RequestBody Appointment appointmentRequest) {
		Appointment appointment = new Appointment();
		appointment.setPatient(appointmentRequest.getPatient());
		appointment.setUser(appointmentRequest.getUser());
		appointment.setUrgency(appointmentRequest.getUrgency());
		appointment.setPrice(appointmentRequest.getPrice());
		appointment.setAppointmentDate(appointmentRequest.getAppointmentDate());
		appointment.setAppointmentHour(appointmentRequest.getAppointmentHour());
		appointment.setRegistryDate(appointmentRequest.getRegistryDate());
		appointment.setOrderNumber(appointmentRequest.getOrderNumber());

		appointmentRepository.save(appointment);
		return new ResponseEntity<Appointment>(appointment, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Appointment> updateAppointment(@PathVariable("id") long id,
			@RequestBody Appointment appointmentRequest) {
		Optional<Appointment> appointment = appointmentRepository.findById(id);

		if (appointment.isPresent()) {
			Appointment appointmentEntity = appointment.get();
			appointmentEntity.setPatient(appointmentRequest.getPatient());
			appointmentEntity.setUser(appointmentRequest.getUser());
			appointmentEntity.setUrgency(appointmentRequest.getUrgency());
			appointmentEntity.setPrice(appointmentRequest.getPrice());
			appointmentEntity.setAppointmentDate(appointmentRequest.getAppointmentDate());
			appointmentEntity.setAppointmentHour(appointmentRequest.getAppointmentHour());
			appointmentEntity.setRegistryDate(appointmentRequest.getRegistryDate());
			appointmentEntity.setOrderNumber(appointmentRequest.getOrderNumber());

			appointmentRepository.save(appointmentEntity);
			return new ResponseEntity<Appointment>(appointmentEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Appointment>(HttpStatus.NOT_FOUND);
		}
	}

}
