package com.rco.clinica.appointment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "cita")
@Data
public class Appointment implements Serializable {

	private static final long serialVersionUID = 1680175772414677691L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "paciente_DNI")
	private Long patient;

	@Column(name = "medico_id")
	private Long user;

	@Column(name = "urgencia")
	private String urgency;

	@Column(name = "pvp")
	private Double price;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_programada")
	private Date appointmentDate;

	@Column(name = "hora_programada")
	private String appointmentHour;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_registro")
	private Date registryDate;

	@Column(name = "numero_orden")
	private Long orderNumber;
}
