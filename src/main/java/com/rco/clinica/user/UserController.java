package com.rco.clinica.user;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("users")
public class UserController {

	private UserRepository userRepository;

	@Autowired
	UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<User>> getUsers() {
		return new ResponseEntity<Iterable<User>>(userRepository.getAllUsersExceptAdmin(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			return new ResponseEntity<User>(user.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("")
	public ResponseEntity<List<User>> getUsersByNameOrUsername(@RequestParam(required = false) String text) {
		return new ResponseEntity<List<User>>(
				userRepository.getAllUsersFilteredExceptAdmin("%" + text + "%"), HttpStatus.OK);
	}
	
	@GetMapping("/appointments-per-day")
	public ResponseEntity<Long> getAppointmentsPerDayByDoctor() {
		Long appointments = userRepository.getAppointmentsPerDay();
		if (appointments == null) {
			appointments = (long) 0;
		}
		return new ResponseEntity<Long>(appointments, HttpStatus.OK);
	}
	
	@GetMapping("/consultations-per-day")
	public ResponseEntity<Long> getConsultationsPerDayByDoctor() {
		Long consultations = userRepository.getMedicalConsultationsDonePerDay();
		if (consultations == null) {
			consultations = (long) 0;
		}
		return new ResponseEntity<Long>(consultations, HttpStatus.OK);
	}
	
	@GetMapping("/patients-per-doctor")
	public ResponseEntity<Long> getAmountOfPatientsPerDoctor() {
		Long patients = userRepository.countPatients();
		if (patients == null) {
			patients = (long) 0;
		}
		return new ResponseEntity<Long>(patients, HttpStatus.OK);
	}

	@GetMapping("patient-appointments-per-doctor")
	public ResponseEntity<List<Map>> getPatientAppointmentsPerDoctorForTable() {
		List<Map> appointmentList = userRepository.getPatientAppointmentsPerDay();
		return new ResponseEntity<List<Map>>(appointmentList, HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<User> createUser(@RequestBody User userRequest) {
		User user = new User();
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setUser(userRequest.getUser());
		user.setPassword(userRequest.getPassword());
		user.setSpeciality(userRequest.getSpeciality());
		user.setSex(userRequest.getSex());

		userRepository.save(user);
		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User userRequest) {
		Optional<User> user = userRepository.findById(id);

		if (user.isPresent()) {
			User userEntity = user.get();
			userEntity.setName(userRequest.getName());
			userEntity.setEmail(userRequest.getEmail());
			userEntity.setUser(userRequest.getUser());
			userEntity.setPassword(userRequest.getPassword());
			userEntity.setSpeciality(userRequest.getSpeciality());
			userEntity.setSex(userRequest.getSex());

			userRepository.save(userEntity);
			return new ResponseEntity<User>(userEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
	}

}
