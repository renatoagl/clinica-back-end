package com.rco.clinica.user;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Long> {

	@Query(value = "SELECT * FROM usuario WHERE usuario NOT LIKE 'admin'", nativeQuery = true)
	List<User> getAllUsersExceptAdmin();

	@Query(value = "SELECT * FROM usuario AS u WHERE (:text IS NULL OR (u.nombre LIKE CONCAT('%',:text,'%') OR u.usuario LIKE CONCAT('%',:text,'%'))) AND (u.usuario NOT LIKE 'admin')", nativeQuery = true)
	List<User> getAllUsersFilteredExceptAdmin(@Param("text") String text);

	// TODO: Cambiar a médico logeado al final de la query
	@Query(value = "SELECT COUNT(id) FROM cita WHERE fecha_programada = CURRENT_DATE() AND hora_programada > CURRENT_TIME() GROUP BY medico_id HAVING medico_id = 17", nativeQuery = true)
	Long getAppointmentsPerDay();

	// TODO: Cambiar a médico logeado al final de la query
	@Query(value = "SELECT COUNT(cm.cita_id) FROM consultamedica cm JOIN cita ci ON cm.cita_id=ci.id WHERE ci.fecha_programada = CURRENT_DATE() AND cm.hora_finalizada < CURRENT_TIMESTAMP() GROUP BY ci.medico_id HAVING medico_id=17", nativeQuery = true)
	Long getMedicalConsultationsDonePerDay();
	
	// TODO: Cambiar a médico logeado al final de la query
	@Query(value = "SELECT COUNT(DISTINCT(paciente_DNI)) FROM cita WHERE medico_id = 17", nativeQuery = true)
	Long countPatients();

	// TODO: Cambiar a médico logeado al final de la query
	@Query(value = "SELECT p.nombre, p.num_expediente, c.urgencia, c.fecha_programada, c.hora_programada, c.numero_orden from paciente p join cita c on p.DNI=c.paciente_DNI where c.fecha_programada=current_date() and c.medico_id=17", nativeQuery = true)
	List<Map> getPatientAppointmentsPerDay();
}
