package com.rco.clinica.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "usuario")
@Data
public class User implements Serializable {

	private static final long serialVersionUID = 5488770789875505410L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "nombre")
	private String name;

	@Column(name = "correo")
	private String email;

	@Column(name = "usuario", unique = true)
	private String user;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Column(name = "password")
	private String password;

	@Column(name = "especialidad")
	private String speciality;

	@Column(name = "sexo")
	private String sex;

	@Column(name = "remember_token")
	private String token;
}
