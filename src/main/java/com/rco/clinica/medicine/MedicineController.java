package com.rco.clinica.medicine;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rco.clinica.clinicalhistoryguide.ClinicalHistoryGuide;

@RestController
@CrossOrigin
@RequestMapping("medicines")
public class MedicineController {
	private MedicineRepository medicineRepository;

	@Autowired
	MedicineController(MedicineRepository medicineRepository) {
		this.medicineRepository = medicineRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Medicine>> getMedicines() {
		return new ResponseEntity<Iterable<Medicine>>(medicineRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Medicine> getMedicineById(@PathVariable("id") Integer id) {
		Optional<Medicine> medicine = medicineRepository.findById(id);
		if (medicine.isPresent()) {
			return new ResponseEntity<Medicine>(medicine.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Medicine>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("")
	public ResponseEntity<List<Medicine>> getMedicineFiltered(@RequestParam(required= false) String text) {
		return new ResponseEntity<List<Medicine>>(medicineRepository.searchFilters(text), HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<Medicine> createMedicine(@RequestBody Medicine medicineRequest) {
		Medicine medicine = new Medicine();
		medicine.setName(medicineRequest.getName());

		medicineRepository.save(medicine);
		return new ResponseEntity<Medicine>(medicine, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Medicine> updateMedicine(@PathVariable("id") Integer id, @RequestBody Medicine medicineRequest) {
		Optional<Medicine> medicine = medicineRepository.findById(id);

		if (medicine.isPresent()) {
			Medicine medicineEntity = medicine.get();
			medicineEntity.setName(medicineRequest.getName());

			medicineRepository.save(medicineEntity);
			return new ResponseEntity<Medicine>(medicineEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Medicine>(HttpStatus.NOT_FOUND);
		}
	}
}
