package com.rco.clinica.medicine;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MedicineRepository extends CrudRepository<Medicine, Integer> {

	@Query(value = "SELECT * FROM medicamento AS m WHERE (:text IS NULL OR (m.nombre LIKE CONCAT ('%',:text,'%')))", nativeQuery = true)
	List<Medicine> searchFilters(@Param("text") String text);

}
