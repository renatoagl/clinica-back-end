package com.rco.clinica.prescription;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("prescriptions")
public class PrescriptionController {

	private PrescriptionRepository prescriptionRepository;

	@Autowired
	PrescriptionController(PrescriptionRepository prescriptionRepository) {
		this.prescriptionRepository = prescriptionRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Prescription>> getPrescriptions() {
		return new ResponseEntity<Iterable<Prescription>>(prescriptionRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Prescription> getPrescriptionById(@PathVariable("id") long id) {
		Optional<Prescription> prescription = prescriptionRepository.findById(id);
		if (prescription.isPresent()) {
			return new ResponseEntity<Prescription>(prescription.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Prescription>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<Prescription> createPrescription(@RequestBody Prescription prescriptionRequest) {
		Prescription prescription = new Prescription();
		prescription.setPatient(prescriptionRequest.getPatient());
		prescription.setUser(prescriptionRequest.getUser());
		prescription.setAppointment(prescriptionRequest.getAppointment());
		prescription.setInstructions(prescriptionRequest.getInstructions());

		prescriptionRepository.save(prescription);
		return new ResponseEntity<Prescription>(prescription, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Prescription> updatePrescription(@PathVariable("id") long id,
			@RequestBody Prescription prescriptionRequest) {
		Optional<Prescription> prescription = prescriptionRepository.findById(id);

		if (prescription.isPresent()) {
			Prescription prescriptionEntity = prescription.get();
			prescriptionEntity.setPatient(prescriptionRequest.getPatient());
			prescriptionEntity.setUser(prescriptionRequest.getUser());
			prescriptionEntity.setAppointment(prescriptionRequest.getAppointment());
			prescriptionEntity.setInstructions(prescriptionRequest.getInstructions());

			prescriptionRepository.save(prescriptionEntity);
			return new ResponseEntity<Prescription>(prescriptionEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Prescription>(HttpStatus.NOT_FOUND);
		}
	}

}
