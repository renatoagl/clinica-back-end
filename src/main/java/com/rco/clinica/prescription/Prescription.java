package com.rco.clinica.prescription;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "recetamedica")
@Data
public class Prescription implements Serializable {

	private static final long serialVersionUID = -3513245039710747663L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "paciente_DNI")
	private Long patient;

	@Column(name = "medico_id")
	private Long user;

	@Column(name = "cita_id")
	private Long appointment;

	@Column(name = "instrucciones")
	private String instructions;
}
