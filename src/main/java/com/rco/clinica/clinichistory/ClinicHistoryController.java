package com.rco.clinica.clinichistory;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("clinic-stories")
public class ClinicHistoryController {

	private ClinicHistoryRepository clinicHistoryRepository;

	@Autowired
	ClinicHistoryController(ClinicHistoryRepository clinicHistoryRepository) {
		this.clinicHistoryRepository = clinicHistoryRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<ClinicHistory>> getClinicHistorys() {
		return new ResponseEntity<Iterable<ClinicHistory>>(clinicHistoryRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ClinicHistory> getClinicHistoryById(@PathVariable("id") long id) {
		Optional<ClinicHistory> clinicHistory = clinicHistoryRepository.findById(id);
		if (clinicHistory.isPresent()) {
			return new ResponseEntity<ClinicHistory>(clinicHistory.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<ClinicHistory>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<ClinicHistory> createClinicHistory(@RequestBody ClinicHistory clinicHistoryRequest) {
		ClinicHistory clinicHistory = new ClinicHistory();
		clinicHistory.setPatient(clinicHistoryRequest.getPatient());
		clinicHistory.setUser(clinicHistoryRequest.getUser());
		clinicHistory.setAppointment(clinicHistoryRequest.getAppointment());
		clinicHistory.setPathologicalHistories(clinicHistoryRequest.getPathologicalHistories());
		clinicHistory.setNonPathologicalHistories(clinicHistoryRequest.getNonPathologicalHistories());
		clinicHistory.setGynecologicalHistories(clinicHistoryRequest.getGynecologicalHistories());
		clinicHistory.setFamilyHistories(clinicHistoryRequest.getFamilyHistories());
		clinicHistory.setOtherHistories(clinicHistoryRequest.getOtherHistories());

		clinicHistoryRepository.save(clinicHistory);
		return new ResponseEntity<ClinicHistory>(clinicHistory, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<ClinicHistory> updateClinicHistory(@PathVariable("id") long id,
			@RequestBody ClinicHistory clinicHistoryRequest) {
		Optional<ClinicHistory> clinicHistory = clinicHistoryRepository.findById(id);

		if (clinicHistory.isPresent()) {
			ClinicHistory clinicHistoryEntity = clinicHistory.get();
			clinicHistoryEntity.setPatient(clinicHistoryRequest.getPatient());
			clinicHistoryEntity.setUser(clinicHistoryRequest.getUser());
			clinicHistoryEntity.setAppointment(clinicHistoryRequest.getAppointment());
			clinicHistoryEntity.setPathologicalHistories(clinicHistoryRequest.getPathologicalHistories());
			clinicHistoryEntity.setNonPathologicalHistories(clinicHistoryRequest.getNonPathologicalHistories());
			clinicHistoryEntity.setGynecologicalHistories(clinicHistoryRequest.getGynecologicalHistories());
			clinicHistoryEntity.setFamilyHistories(clinicHistoryRequest.getFamilyHistories());
			clinicHistoryEntity.setOtherHistories(clinicHistoryRequest.getOtherHistories());

			clinicHistoryRepository.save(clinicHistoryEntity);
			return new ResponseEntity<ClinicHistory>(clinicHistoryEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<ClinicHistory>(HttpStatus.NOT_FOUND);
		}
	}

}
