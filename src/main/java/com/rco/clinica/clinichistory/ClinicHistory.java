package com.rco.clinica.clinichistory;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "historiaclinica")
@Data
public class ClinicHistory implements Serializable {

	private static final long serialVersionUID = 2202920332468950463L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "paciente_DNI")
	private Long patient;

	@Column(name = "medico_id")
	private Long user;

	@Column(name = "cita_id")
	private Long appointment;

	@Column(name = "ant_patologico")
	private String pathologicalHistories;

	@Column(name = "ant_no_patologico")
	private String NonPathologicalHistories;

	@Column(name = "ant_ginecologico")
	private String gynecologicalHistories;

	@Column(name = "ant_familiar")
	private String familyHistories;

	@Column(name = "ant_otros")
	private String otherHistories;
}
