package com.rco.clinica.clinichistory;

import org.springframework.data.repository.CrudRepository;

public interface ClinicHistoryRepository extends CrudRepository<ClinicHistory, Long>{

}
