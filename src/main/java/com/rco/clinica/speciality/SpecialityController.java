package com.rco.clinica.speciality;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("specialities")
public class SpecialityController {

	private SpecialityRepository specialityRepository;

	@Autowired
	SpecialityController(SpecialityRepository specialityRepository) {
		this.specialityRepository = specialityRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<Speciality>> getSpecialitys() {
		return new ResponseEntity<Iterable<Speciality>>(specialityRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Speciality> getSpecialityById(@PathVariable("id") long id) {
		Optional<Speciality> speciality = specialityRepository.findById(id);
		if (speciality.isPresent()) {
			return new ResponseEntity<Speciality>(speciality.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<Speciality>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<Speciality> createSpeciality(@RequestBody Speciality specialityRequest) {
		Speciality speciality = new Speciality();
		speciality.setName(specialityRequest.getName());

		specialityRepository.save(speciality);
		return new ResponseEntity<Speciality>(speciality, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Speciality> updateSpeciality(@PathVariable("id") long id,
			@RequestBody Speciality specialityRequest) {
		Optional<Speciality> speciality = specialityRepository.findById(id);

		if (speciality.isPresent()) {
			Speciality specialityEntity = speciality.get();

			specialityRepository.save(specialityEntity);
			return new ResponseEntity<Speciality>(specialityEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<Speciality>(HttpStatus.NOT_FOUND);
		}
	}

}
