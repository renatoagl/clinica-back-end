package com.rco.clinica.speciality;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "especialidad")
@Data
public class Speciality implements Serializable {

	private static final long serialVersionUID = -7705432443504541260L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "nombre")
	private String name;

}
