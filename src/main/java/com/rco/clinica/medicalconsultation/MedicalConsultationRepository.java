package com.rco.clinica.medicalconsultation;

import org.springframework.data.repository.CrudRepository;

public interface MedicalConsultationRepository extends CrudRepository<MedicalConsultation, Long> {

}
