package com.rco.clinica.medicalconsultation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "consultamedica")
@Data
public class MedicalConsultation implements Serializable {

	private static final long serialVersionUID = 4734541133046793839L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "paciente_DNI")
	private Long patient;

	@Column(name = "medico_id")
	private Long user;

	@Column(name = "cita_id")
	private Long appointment;

	@Column(name = "motivo_consulta")
	private String reason;

	@Column(name = "diagnostico_clinico")
	private String diagnostic;

	@Column(name = "altura")
	private String height;

	@Column(name = "peso")
	private String weight;

	@Column(name = "IMC")
	private String IMC;

	@Column(name = "temperatura")
	private String temperature;

	@Column(name = "FR")
	private String FR;

	@Column(name = "PA")
	private String PA;

	@Column(name = "FC")
	private String FC;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "hora_finalizada", nullable = false)
	private Date endHour;
}
