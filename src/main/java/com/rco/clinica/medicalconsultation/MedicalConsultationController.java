package com.rco.clinica.medicalconsultation;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("medical-consultations")
public class MedicalConsultationController {

	private MedicalConsultationRepository medicalConsultationRepository;

	@Autowired
	MedicalConsultationController(MedicalConsultationRepository medicalConsultationRepository) {
		this.medicalConsultationRepository = medicalConsultationRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<MedicalConsultation>> getMedicalConsultations() {
		return new ResponseEntity<Iterable<MedicalConsultation>>(medicalConsultationRepository.findAll(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<MedicalConsultation> getMedicalConsultationById(@PathVariable("id") long id) {
		Optional<MedicalConsultation> medicalConsultation = medicalConsultationRepository.findById(id);
		if (medicalConsultation.isPresent()) {
			return new ResponseEntity<MedicalConsultation>(medicalConsultation.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<MedicalConsultation>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<MedicalConsultation> createMedicalConsultation(
			@RequestBody MedicalConsultation medicalConsultationRequest) {
		MedicalConsultation medicalConsultation = new MedicalConsultation();
		medicalConsultation.setPatient(medicalConsultationRequest.getPatient());
		medicalConsultation.setUser(medicalConsultationRequest.getUser());
		medicalConsultation.setAppointment(medicalConsultationRequest.getAppointment());
		medicalConsultation.setReason(medicalConsultationRequest.getReason());
		medicalConsultation.setDiagnostic(medicalConsultationRequest.getDiagnostic());
		medicalConsultation.setHeight(medicalConsultationRequest.getHeight());
		medicalConsultation.setWeight(medicalConsultationRequest.getWeight());
		medicalConsultation.setIMC(medicalConsultationRequest.getIMC());
		medicalConsultation.setTemperature(medicalConsultationRequest.getTemperature());
		medicalConsultation.setFR(medicalConsultationRequest.getFR());
		medicalConsultation.setPA(medicalConsultationRequest.getPA());
		medicalConsultation.setFC(medicalConsultationRequest.getFC());
		medicalConsultation.setEndHour(medicalConsultationRequest.getEndHour());

		medicalConsultationRepository.save(medicalConsultation);
		return new ResponseEntity<MedicalConsultation>(medicalConsultation, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<MedicalConsultation> updateMedicalConsultation(@PathVariable("id") long id,
			@RequestBody MedicalConsultation medicalConsultationRequest) {
		Optional<MedicalConsultation> medicalConsultation = medicalConsultationRepository.findById(id);

		if (medicalConsultation.isPresent()) {
			MedicalConsultation medicalConsultationEntity = medicalConsultation.get();
			medicalConsultationEntity.setPatient(medicalConsultationRequest.getPatient());
			medicalConsultationEntity.setUser(medicalConsultationRequest.getUser());
			medicalConsultationEntity.setAppointment(medicalConsultationRequest.getAppointment());
			medicalConsultationEntity.setReason(medicalConsultationRequest.getReason());
			medicalConsultationEntity.setDiagnostic(medicalConsultationRequest.getDiagnostic());
			medicalConsultationEntity.setHeight(medicalConsultationRequest.getHeight());
			medicalConsultationEntity.setWeight(medicalConsultationRequest.getWeight());
			medicalConsultationEntity.setIMC(medicalConsultationRequest.getIMC());
			medicalConsultationEntity.setTemperature(medicalConsultationRequest.getTemperature());
			medicalConsultationEntity.setFR(medicalConsultationRequest.getFR());
			medicalConsultationEntity.setPA(medicalConsultationRequest.getPA());
			medicalConsultationEntity.setFC(medicalConsultationRequest.getFC());
			medicalConsultationEntity.setEndHour(new Date());

			medicalConsultationRepository.save(medicalConsultationEntity);
			return new ResponseEntity<MedicalConsultation>(medicalConsultationEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<MedicalConsultation>(HttpStatus.NOT_FOUND);
		}
	}

}
