package com.rco.clinica.clinicalhistoryguide;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ClinicalHistoryGuideRepository extends CrudRepository<ClinicalHistoryGuide, Long> {

	@Query(value = "SELECT tipo FROM historiaclinicaguia GROUP BY tipo", nativeQuery = true)
	List<String> getTypes();

	@Query(value = "SELECT * FROM historiaclinicaguia AS h WHERE (:text IS NULL OR (h.nombre LIKE CONCAT ('%',:text,'%'))) AND (:type IS NULL OR h.tipo=:type)", nativeQuery = true)
	List<ClinicalHistoryGuide> searchFilters(@Param("text") String text, @Param("type") String type);

}
