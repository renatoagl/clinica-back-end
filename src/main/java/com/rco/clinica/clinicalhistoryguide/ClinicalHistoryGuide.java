package com.rco.clinica.clinicalhistoryguide;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "historiaclinicaguia")
@Data
public class ClinicalHistoryGuide implements Serializable {

	private static final long serialVersionUID = -3482613043301186415L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "nombre")
	private String name;

	@Column(name = "tipo")
	private String type;
}
