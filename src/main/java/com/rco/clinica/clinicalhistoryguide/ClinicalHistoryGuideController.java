package com.rco.clinica.clinicalhistoryguide;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("clinical-history-guides")
public class ClinicalHistoryGuideController {

	private ClinicalHistoryGuideRepository clinicalHistoryGuideRepository;

	@Autowired
	ClinicalHistoryGuideController(ClinicalHistoryGuideRepository clinicalHistoryGuideRepository) {
		this.clinicalHistoryGuideRepository = clinicalHistoryGuideRepository;
	}

	@GetMapping("/all")
	public ResponseEntity<Iterable<ClinicalHistoryGuide>> getClinicalHistoryGuides() {
		return new ResponseEntity<Iterable<ClinicalHistoryGuide>>(clinicalHistoryGuideRepository.findAll(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ClinicalHistoryGuide> getClinicalHistoryGuideById(@PathVariable("id") long id) {
		Optional<ClinicalHistoryGuide> clinicalHistoryGuide = clinicalHistoryGuideRepository.findById(id);
		if (clinicalHistoryGuide.isPresent()) {
			return new ResponseEntity<ClinicalHistoryGuide>(clinicalHistoryGuide.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<ClinicalHistoryGuide>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("")
	public ResponseEntity<List<ClinicalHistoryGuide>> getClinicalHistoryGuidesFiltered(@RequestParam(required= false) String text, @RequestParam(required= false) String type) {
		if (type.equals("undefined")) {
			type = null;
		}
		
		return new ResponseEntity<List<ClinicalHistoryGuide>>(clinicalHistoryGuideRepository.searchFilters(text, type), HttpStatus.OK);
	}
	
	@GetMapping("/types")
	public ResponseEntity<List<String>> getTypes() {
		return new ResponseEntity<List<String>>(clinicalHistoryGuideRepository.getTypes(), HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<ClinicalHistoryGuide> createClinicalHistoryGuide(
			@RequestBody ClinicalHistoryGuide clinicalHistoryGuideRequest) {
		ClinicalHistoryGuide clinicalHistoryGuide = new ClinicalHistoryGuide();
		clinicalHistoryGuide.setName(clinicalHistoryGuideRequest.getName());
		clinicalHistoryGuide.setType(clinicalHistoryGuideRequest.getType());

		clinicalHistoryGuideRepository.save(clinicalHistoryGuide);
		return new ResponseEntity<ClinicalHistoryGuide>(clinicalHistoryGuide, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<ClinicalHistoryGuide> updateClinicalHistoryGuide(@PathVariable("id") long id,
			@RequestBody ClinicalHistoryGuide clinicalHistoryGuideRequest) {
		Optional<ClinicalHistoryGuide> clinicalHistoryGuide = clinicalHistoryGuideRepository.findById(id);

		if (clinicalHistoryGuide.isPresent()) {
			ClinicalHistoryGuide clinicalHistoryGuideEntity = clinicalHistoryGuide.get();
			clinicalHistoryGuideEntity.setName(clinicalHistoryGuideRequest.getName());
			clinicalHistoryGuideEntity.setType(clinicalHistoryGuideRequest.getType());

			clinicalHistoryGuideRepository.save(clinicalHistoryGuideEntity);
			return new ResponseEntity<ClinicalHistoryGuide>(clinicalHistoryGuideEntity, HttpStatus.OK);
		} else {
			return new ResponseEntity<ClinicalHistoryGuide>(HttpStatus.NOT_FOUND);
		}
	}

}
