package com.rco.clinica.jasper.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
	
	// Connection
    private Connection con = null;
    
    // Conexion LOCAL
    private final String url = "jdbc:mysql://localhost:3306/clinica?serverTimezone=Europe/Madrid";
    private final String user = "root";
    private final String password = "";
    
    public Connection getConexion() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = (Connection) DriverManager.getConnection(this.url, this.user, this.password);
            System.out.println("Conectado a la BBDD");
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return con;
    }
    
    public Connection desconexion() {
        try {
            con.close();
            System.out.println("Desconectado de la BBDD");
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    
}
