package com.rco.clinica.jasper.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rco.clinica.jasper.commons.JasperReportManager;
import com.rco.clinica.jasper.enums.EnumReportType;
import com.rco.clinica.jasper.model.ReportDTO;
import com.rco.clinica.jasper.service.api.ReportServiceAPI;

import net.sf.jasperreports.engine.JRException;

@Service
public class ReportServiceImpl implements ReportServiceAPI {

	@Autowired
	private JasperReportManager reportManager;

	@Autowired
	private DataSource dataSource;

	@Override
	public ReportDTO getReport(Map<String, Object> params) throws JRException, IOException, SQLException {
		String fileName = params.get("document").toString();
		ReportDTO dto = new ReportDTO();
		String extension = params.get("type").toString().equalsIgnoreCase(EnumReportType.EXCEL.name()) ? ".xlsx"
				: ".pdf";
		dto.setFileName(fileName + extension);

		ByteArrayOutputStream stream = reportManager.export(fileName, params.get("type").toString(), params,
				dataSource.getConnection());

		byte[] bs = stream.toByteArray();
		dto.setStream(new ByteArrayInputStream(bs));
		dto.setLength(bs.length);

		return dto;
	}

}
