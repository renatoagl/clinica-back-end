package com.rco.clinica.jasper.service.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import com.rco.clinica.jasper.model.ReportDTO;

import net.sf.jasperreports.engine.JRException;

public interface ReportServiceAPI {

	ReportDTO getReport(Map<String, Object> params) throws JRException, IOException, SQLException;

}
