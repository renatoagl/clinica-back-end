package com.rco.clinica.jasper.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rco.clinica.jasper.service.api.ReportServiceAPI;
import com.rco.clinica.medicine.Medicine;
import com.rco.clinica.medicine.MedicineRepository;
import com.rco.clinica.patient.Patient;
import com.rco.clinica.patient.PatientRepository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RestController
@CrossOrigin
@RequestMapping("/reports")
public class ReportController {

	@Autowired
	private ReportServiceAPI reportServiceAPI;
	@Autowired
	private MedicineRepository medicineRepository;
	@Autowired
	private PatientRepository patientRepository;

	// Example(1):
	// http://localhost:8080/reports/download?type=PDF&document=Medicines
	// Example(2): http://localhost:8080/reports/download?type=PDF&document=Patients
//	@GetMapping(path = "/download")
//	public ResponseEntity<Resource> download(@RequestParam Map<String, Object> params)
//			throws JRException, IOException, SQLException {
//		ReportDTO dto = reportServiceAPI.getReport(params);
//
//		InputStreamResource streamResource = new InputStreamResource(dto.getStream());
//		MediaType mediaType = null;
//		if (params.get("type").toString().equalsIgnoreCase(EnumReportType.EXCEL.name())) {
//			mediaType = MediaType.APPLICATION_OCTET_STREAM;
//		} else {
//			mediaType = MediaType.APPLICATION_PDF;
//		}
//
//		return ResponseEntity.ok().header("Content-Disposition", "inline; filename=\"" + dto.getFileName() + "\"")
//				.contentLength(dto.getLength()).contentType(mediaType).body(streamResource);
//	}

	@GetMapping(path = "/download")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Resource> download(@RequestParam Map<String, Object> params)
			throws FileNotFoundException, JRException {
		String path = null;
		if (System.getProperty("user.home").contains("home")) { 
			path = System.getProperty("user.home") + "/Descargas";
		} else {
			path = System.getProperty("user.home") + "/Downloads";
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE);
		headers.add("Cache-Control", "private");

		if (params.get("document").equals("Medicines")) {
			List<Medicine> medicines = (List) medicineRepository.findAll();
			File file = ResourceUtils.getFile("classpath:reports/Medicines.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(medicines, false);
			Map<String, Object> parameters = new HashMap<>();
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/Medicines.pdf");
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

			return ResponseEntity.ok().headers(headers).contentLength(file.length()).body(resource);
		} else {
			List<Patient> patients = (List) patientRepository.findAll();
			File file = ResourceUtils.getFile("classpath:reports/Patients.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(patients, false);
			Map<String, Object> parameters = new HashMap<>();
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/Patients.pdf");
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

			return ResponseEntity.ok().headers(headers).contentLength(file.length()).body(resource);
		}
	}

	@GetMapping("/download-manual")
	public ResponseEntity<byte[]> downloadManual() throws IOException {
		String filename = "Manual.pdf";
		byte[] filePDF = Files.readAllBytes(Paths.get("src/main/resources/documents/Manual.pdf"));		
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.parseMediaType("application/pdf"));
	    headers.add("content-disposition", "attachment;filename=" + filename);
	    headers.setContentDispositionFormData(filename, filename);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	    ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(filePDF, headers, HttpStatus.OK);
	    return response;
	}
}
