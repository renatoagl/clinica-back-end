package com.rco.clinica.jasper.model;

import java.io.ByteArrayInputStream;

import lombok.Data;

@Data
public class ReportDTO {

	private String fileName;
	private ByteArrayInputStream stream;
	private int length;
	
}
